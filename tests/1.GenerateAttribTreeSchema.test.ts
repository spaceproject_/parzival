import { Parseable, ValidateProperty, objectSchemaFrom } from "../lib";

// [Jest test] GenerateFlatSchema.test.ts
test("GenerateAttribTreeSchema", async () => {
	@Parseable()
	class SampleChild {
		@ValidateProperty({
			type: "string",
		})
		foo: string;

		@ValidateProperty({
			type: "number",
		})
		bar: number;

		@ValidateProperty({
			type: "boolean",
		})
		baz: boolean;
	}

	@Parseable()
	class SampleObject {

		@ValidateProperty({
			type: "object",
			recurse: true,
			className: "SampleChild",
		})
		child: SampleChild;

		@ValidateProperty({
			type: "string",
		})
		foo: string;
	}

	// [Jest test] GenerateAttribTreeSchema.test.ts
	const schema = objectSchemaFrom(SampleObject);
	//console.log(schema);

	// Assert that the schema is correct
	expect(schema.objectName).toBe("SampleObject");
	expect(schema.properties.length).toBe(1);
	expect(schema.children.length).toBe(1);
	expect(schema.children[0][1].objectName).toBe("SampleChild");
	expect(schema.children[0][1].properties.length).toBe(3);
});
import { Parseable, ValidateProperty, objectSchemaFrom } from "../lib";

// [Jest test] GenerateFlatSchema.test.ts
test("GenerateFlatSchema", () => {
	@Parseable()
	class SampleParseableClass {
		@ValidateProperty({
			type: "string",
		})
		name: string;

		@ValidateProperty({
			type: "number",
		})
		age: number;

		@ValidateProperty({
			type: "boolean",
		})
		isActive: boolean;
	}

	const schema = objectSchemaFrom(SampleParseableClass);
	expect(schema.objectName).toBe("SampleParseableClass");
	expect(schema.properties.length).toBe(3);
});
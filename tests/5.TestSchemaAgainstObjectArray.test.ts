import { Parseable, ValidateProperty, objectSchemaFrom, validateObject } from "../lib";

// [Jest test] TestSchemaAgainstObjectArray.test.ts
test("TestSchemaAgainstObjectArray", async () => {
	@Parseable()
	class SampleParseableOptionalClass {
		@ValidateProperty({
			type: "string",
			optional: true,
		})
		str?: string;

		@ValidateProperty({
			type: "number",
			optional: true,
		})
		num?: number;

		@ValidateProperty({
			type: "boolean",
			optional: true,
		})
		bool?: boolean;
	}

	@Parseable()
	class SampleParseableArraySubClass {
		@ValidateProperty({
			type: "string",
		})
		str: string;

		@ValidateProperty({
			type: "number",
			min: 0,
			max: 100,
		})
		num: number;

		@ValidateProperty({
			type: "boolean",
		})
		bool: boolean;

		@ValidateProperty({
			type: "string",
			optional: true,
		})
		opt?: string;

		@ValidateProperty({
			type: "array",
			subTypeOptions: {
				type: "string",
			},
		})
		strArray: string[];

		@ValidateProperty({
			type: "string",
			match: /^Hello World$/,
		})
		match: string;
	}

	@Parseable()
	class SampleParseableArrayParentClass {
		@ValidateProperty({
			type: "array",
			subTypeOptions: {
				type: "object",
				className: "SampleParseableArraySubClass",
				recurse: true,
			},
		})
		child: SampleParseableArraySubClass[];

		@ValidateProperty({
			type: "string",
		})
		str: string;

		@ValidateProperty({
			type: "object",
			className: "SampleParseableOptionalClass",
			recurse: true,
			optional: true,
		})
		optObj?: SampleParseableOptionalClass;
	}

	// Objects and Validation
	const schema = objectSchemaFrom(SampleParseableArrayParentClass);
	// Matching object
	const object0 = {
		child: [{
			str: "Hello World",
			num: 50,
			bool: true,
			strArray: ["Hello", "World"],
			match: "Hello World",
		}],
		str: "Hello World",
		// here we have an optional object
	};
	// Non-matching object by wrong type
	const object1 = {
		child: [{
			str: "Hello World",
			num: 50,
			bool: true,
			strArray: ["Hello", "World"],
			match: "Hello World",
		}],
		str: 50,
	};
	// Non-matching object by missing property
	const object2 = {
		child: [{
			str: "Hello World",
			num: 50,
			bool: true,
			strArray: ["Hello", "World"],
			match: "Hello World",
		}],
	};
	const validation0 = validateObject(object0, schema);
	const validation1 = validateObject(object1, schema);
	const validation2 = validateObject(object2, schema);
	expect(validation0).toBe(true);
	expect(validation1).toBe(false);
	expect(validation2).toBe(false);
});

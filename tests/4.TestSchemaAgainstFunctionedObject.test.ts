import { Parseable, ValidateProperty, objectSchemaFrom, validateObject } from "../lib";

test("TestSchemaAgainstFunctionedObject", () => {

	@Parseable()
	class SampleParseableFunctionClass {
		@ValidateProperty({
			type: "function",
			validateReturns: false,
			validateArguments: true,
			argumentsTypeOptions: {
				str: {
					type: "string",
				},
				num: {
					type: "number",
				},
				arr: {
					type: "array",
					subTypeOptions: {
						type: "string",
					}
				}
			},
		})
		func: (str: string, num: number, arr: string[]) => void;
	}
	@Parseable()
	class SampleParseableFunctionReturnClass {
		@ValidateProperty({
			type: "function",
			validateReturns: true,
			validateArguments: false,
			returnTypeOptions: {
				type: "string",
			},
		})
		func: (str: string, num: number, arr: string[]) => string;
	}

	const schema = objectSchemaFrom(SampleParseableFunctionClass);
	const schema2 = objectSchemaFrom(SampleParseableFunctionReturnClass);
	// Schema1
	const obj = {
		func: (str: string, num: number, arr: string[]) => {
			console.log("Complete function");
		}
	}
	const obj2 = {};
	const obj3 = {
		func: (num: number, arr: number[]) => {
			console.log("Missing argument");
		}
	}
	// Schema2
	const obj4 = {
		func: (str: string, num: number, arr: string[]) => {
			return "Complete function";
		}
	}
	const obj5 = {
		func: (str: string, num: number, arr: string[]) => {
			return 1;
		}
	}

	// Schema1
	expect(validateObject(obj, schema)).toBe(true);
	expect(validateObject(obj2, schema)).toBe(false);
	//expect(validateObject(obj3, schema)).toBe(false); // TODO: Fix this
	// Schema2
	expect(validateObject(obj4, schema2)).toBe(true);
	//expect(validateObject(obj5, schema2)).toBe(false); // TODO: Add this
});
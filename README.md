# Parzival

A simple Object Parsing Library for TypeScript, using Decorators.

Under the hood it uses [reflect-metadata](https://www.npmjs.com/package/reflect-metadata) to parse the object.
Partially uses a custom object validator and schema assembler, but now its slowly migrating to use [zod](https://www.npmjs.com/package/zod) under the hood.

> [!WARNING]
> This library is still in development, and its API may be broken many times when updates come out.

## Environment Variables
Following some convention, the library uses some environment variables to configure its behavior.

- `PARZIVAL_TEMP_STORAGE_DIR` - The directory where the temporary files are stored. 
  Default: `temp/.parzival` under its node_modules directory.
- `PARZIVAL_ENV` - The environment in which the library is running, mostly used for debugging purposes
  IT WILL MAKE THE LOGS INCREDIBLY VERBOSE if set to "development".
  Default: empty.
  Valid values: `development`, `errors`.
import { ObjectProperty, ObjectSchema } from "../types/ObjectSchema";

/**
 * Validates an object against a schema
 * @param object
 * @param schema ObjectSchema
 * @returns boolean
 */
export function validateObject<T extends unknown = any>(object: any, schema: ObjectSchema): object is T {
	if (process.env.PARZIVAL_ENV === 'development') {
		console.log("Validating object", object, "against schema", schema);
	}
	if (!schema) throw new Error("Schema is undefined or null");
	if (object === undefined || object === null) {
		if (schema.isOptional) return true;
		if (process.env.PARZIVAL_ENV === 'development' || process.env.PARZIVAL_ENV === 'errors')
			console.error("Object is undefined or null");
		return false;
	}
	if (schema.validateName && object.constructor.name !== schema.objectName) {
		if (process.env.PARZIVAL_ENV === 'development' || process.env.PARZIVAL_ENV === 'errors')
			console.error(`Object ${object.constructor.name} does not match schema object name ${schema.objectName}`);
		return false;
	}
	if (!schema.properties) throw new Error("Schema properties are undefined or null");
	for (const property of schema.properties) {
		if (object[property.propertyName] === undefined || object[property.propertyName] === null) {
			if (!property.isOptional) {
				if (process.env.PARZIVAL_ENV === 'development' || process.env.PARZIVAL_ENV === 'errors')
					console.error(`Property ${property.propertyName} is not optional but is missing when validating object ${schema.objectName}`);
				return false;
			}
			else return true;
		}
		if (!validateProperty(object[property.propertyName], property)) {
			if (process.env.PARZIVAL_ENV === 'development' || process.env.PARZIVAL_ENV === 'errors')
				console.error(`Property ${property.propertyName} failed validation when validating object ${schema.objectName}`);
			return false;
		}
	}
	if (schema.recurse) {
		for (const child of schema.children) {
			if (!validateObject(object[child[0]], child[1])) {
				if (process.env.PARZIVAL_ENV === 'development' || process.env.PARZIVAL_ENV === 'errors')
					console.error(`Child ${child[0]} failed validation when validating object ${schema.objectName}`);
				return false;
			}
		}
	}
	return true;
}
export function validateProperty(value: any, property: ObjectProperty): boolean {
	const v = property.schema.safeParse(value);
	if (v.success) {
		//console.debug(`Property ${property.propertyName} passed validation`, v.success);
		return true;
	}
	else {
		const msg = `Property ${property.propertyName} failed validation with error ${v.error.message}`;
		console.error(msg);
		return false;
	}
}
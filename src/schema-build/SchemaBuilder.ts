import { ArrayPropertyOptions, BooleanPropertyOptions, FunctionPropertyOptions, NumberPropertyOptions, ObjectPropertyOptions, StringPropertyOptions, ValidatePropertyOptions } from "../decorators/options/ValidatePropertyOptions";
import { ObjectProperty, ObjectSchema } from "../types/ObjectSchema";
import { PropertyTypesLabel } from "../types/PropertyTypes";
import zod, { ZodAny, ZodArray, ZodBoolean, ZodFunction, ZodNumber, ZodString, ZodTypeAny, ZodUnknown } from "zod";
import { MetadataStorage } from "../util/MetadataStorage";
import { KeyOptionsPair } from "../types/Metadata";
import { PairDif } from "../types/Util";
/**
 * Creates a schema from a class object
 * @param clazz 
 */
// Function declaration without isOptional
export function objectSchemaFrom(clazz: any, className?: string): ObjectSchema {
	return objectSchemaFromWOpt(clazz, className);
}
function objectSchemaFromWOpt(clazz: any, className?: string, isOptional: boolean | undefined = undefined): ObjectSchema {
	if (!clazz && !className) throw new Error("Object is undefined or null");
	if (className && !clazz?.name) {
		clazz = { ...clazz };
		clazz.name = className;
	}
	if (!clazz.name) {
		console.error("Could not get class name for", JSON.stringify(clazz));
		throw new Error("Could not get class name");
	}
	const s = MetadataStorage.getStorage();
	// Check if object is marked for parsing
	if (!s.hasMetadata("parzival:parseable", clazz.name)) throw new Error(`Object ${clazz.name} is not marked for parsing`);
	// Check if Object is marked false for parsing
	if (s.getMetadata("parzival:parseable", clazz.name)[0] === false) throw new Error(`Object ${clazz.name} is marked false for parsing`);

	// Create schema
	const children: PairDif<string, ObjectSchema>[] = [];
	const properties: ObjectProperty[] = [];

	const keys = s.getMetadata("parzival:keys", clazz.name);
	if (!keys) throw new Error("Object has no properties marked for validation");
	const optionsArray = s.getMetadata("parzival:validate:options", clazz.name) as KeyOptionsPair[];
	if (process.env.PARZIVAL_ENV === "development") {
		console.log("Keys", keys);
		console.log("Options", JSON.stringify(optionsArray));
	}
	for (const key of keys) {
		const options = optionsArray?.find((pair) => pair[0] === key)?.[1];
		if (!options) throw new Error(`Property ${key} is marked for validation but has no options`);

		if (options.type === "object") {
			// Check options for recursion
			const recurse = options.recurse;
			if (!recurse) continue;
			// Call recursively
			if (process.env.PARZIVAL_ENV === "development") console.log(`Creating schema for child object ${key}`);
			// TODO: Fix Recursion (passed object tends to be undefined)
			children.push([key, objectSchemaFromWOpt(clazz[key], options.className, options.optional)]);
		}
		else {
			// Get property schema
			const propertySchema = propertySchemaFrom(key, options.type, options);
			properties.push(propertySchema);
		}
	}
	return {
		validateName: false,
		objectName: s.getMetadata("parzival:parseable:name", clazz) || clazz.name,
		properties,
		recurse: true,
		isOptional: isOptional ?? false,
		children
	}
}
/**
 * Intended to receive a property of an object and return a schema for it
 * This doesn't include other objects, only primitives and arrays
 * @param object 
 */
export function propertySchemaFrom(property: any, type: PropertyTypesLabel, options: ValidatePropertyOptions): ObjectProperty {
	if (!property) throw new Error("Property is undefined or null");
	if (!type) throw new Error("Property type is undefined or null");
	if (!options) throw new Error("Property options are undefined or null");
	switch (type) {
		case "string":
			return {
				propertyName: property.name || (property as string).trim().replace(/\s/g, "_"),
				propertyType: type,
				isOptional: options.optional || false,
				schema: stringPropertySchemaFrom(property, options as StringPropertyOptions)
			}
		case "number":
			return {
				propertyName: property.name || (property as string).trim().replace(/\s/g, "_"),
				propertyType: type,
				isOptional: options.optional || false,
				schema: numberPropertySchemaFrom(property, options as NumberPropertyOptions)
			}
		case "boolean":
			return {
				propertyName: property.name || (property as string).trim().replace(/\s/g, "_"),
				propertyType: type,
				isOptional: options.optional || false,
				schema: booleanPropertySchemaFrom(property, options as BooleanPropertyOptions)
			}
		case "array":
			return {
				propertyName: property.name || (property as string).trim().replace(/\s/g, "_"),
				propertyType: type,
				isOptional: options.optional || false,
				schema: arrayPropertySchemaFrom(property, options as ArrayPropertyOptions)
			}
		case "function":
			return {
				propertyName: property.name || (property as string).trim().replace(/\s/g, "_"),
				propertyType: type,
				isOptional: options.optional || false,
				schema: functionPropertySchemaFrom(property, options as FunctionPropertyOptions)
			}
		case "object":
			throw new Error("Object properties should be handled by ObjectSchemaFrom");
		default:
			throw new Error("Invalid type");
	}
}

/**
 * Creates a schema for a string property
 * @param property
 * @param options
 * @returns ObjectProperty of type string
 */
export function stringPropertySchemaFrom(property: any, options: StringPropertyOptions): ZodString {
	// Create schema
	const schema = zod.string();

	// Check if property is required
	if (options.optional) schema.nullish();

	// Check if property has a minimum length
	if (options.minLength) schema.min(options.minLength);

	// Check if property has a maximum length
	if (options.maxLength) schema.max(options.maxLength);

	// Check if property has a match
	if (options.match) schema.regex(options.match);

	return schema;
}

/**
 * Creates a schema for a number property
 * @param property
 * @returns ObjectProperty of type number
 */
export function numberPropertySchemaFrom(property: any, options: NumberPropertyOptions): ZodNumber {
	// Create schema
	const schema = zod.number();

	// Check if property is required
	if (options.optional) schema.nullish();

	// Check if property has a minimum value
	if (options.min) schema.min(options.min);

	// Check if property has a maximum value
	if (options.max) schema.max(options.max);

	return schema;
}

/**
 * Creates a schema for a boolean property
 * @param property
 * @returns ObjectProperty of type boolean
 */
export function booleanPropertySchemaFrom(property: any, options: BooleanPropertyOptions): ZodBoolean {
	// Create schema
	const schema = zod.boolean();

	// Check if property is required
	if (options.optional) schema.nullish();

	return schema;
}


/**
 * Creates a schema for an array property
 * @param property
 * @returns ObjectProperty of type array
 */
export function arrayPropertySchemaFrom(property: any, options: ArrayPropertyOptions) {
	let schema: ZodArray<any>;
	if (!options.subTypeOptions) throw new Error("Array property has no subTypeOptions");
	switch (options.subTypeOptions.type) {
		case "string": {
			schema = zod.array(stringPropertySchemaFrom(property, options.subTypeOptions));
			break;
		}
		case "number": {
			schema = zod.array(numberPropertySchemaFrom(property, options.subTypeOptions));
			break;
		}
		case "boolean": {
			schema = zod.array(booleanPropertySchemaFrom(property, options.subTypeOptions));
			break;
		}
		case "array": {
			schema = zod.array(arrayPropertySchemaFrom(property, options.subTypeOptions));
			break;
		}
		case "object": {
			if (!options.subTypeOptions.className) throw new Error("Array property has no className");
			if (process.env.PARZIVAL_ENV === "development") console.log(`Creating schema for array of objects of type, ${options.subTypeOptions.className}`, options.subTypeOptions);
			schema = zod.array(objectSchemaToZod(objectSchemaFromWOpt(property, options.subTypeOptions.className)));
			break;
		}
		default: {
			throw new Error("Invalid subType");
		}
	}
	// Add array options
	if (options.minLength) schema.min(options.minLength);
	if (options.maxLength) schema.max(options.maxLength);

	return schema;
}

/**
 * Transforms a custom ObjectSchema into a Zod Object Schema
 * @param schema
 * @returns Zod Object Schema
 */
export function objectSchemaToZod(schema: ObjectSchema): ZodTypeAny {
	const obj: { [key: string]: ZodTypeAny } = {};
	for (const property of schema.properties) {
		if (process.env.PARZIVAL_ENV === "development") console.log(`Adding property ${property.propertyName} to schema`);
		if (property.isOptional)
			obj[property.propertyName] = property.schema.optional();
		else
			obj[property.propertyName] = property.schema;
	}
	const zodSchema = zod.object(obj);
	return zodSchema;
}

/**
 * Creates a schema for a function property
 * @param property
 * @returns ObjectProperty of type function
 */
export function functionPropertySchemaFrom(property: any, options: FunctionPropertyOptions): ZodFunction<any, any> {
	const schema = zod.function();
	if (options.optional) schema.nullish();
	// TODO: Add validation for arguments and returns
	/* if (options.validateArguments) {
		if (!options.argumentsTypeOptions) throw new Error("Function property has no subTypeOptions");
		const argsSchemas: any[] = [];
		for (const key of Object.keys(options.argumentsTypeOptions)) {
			const argumentOptions = options.argumentsTypeOptions[key];
			switch (argumentOptions.type) {
				case "string": {
					argsSchemas.push(stringPropertySchemaFrom(property, argumentOptions));
					break;
				}
				case "number": {
					argsSchemas.push(numberPropertySchemaFrom(property, argumentOptions));
					break;
				}
				case "boolean": {
					argsSchemas.push(booleanPropertySchemaFrom(property, argumentOptions));
					break;
				}
				case "array": {
					argsSchemas.push(arrayPropertySchemaFrom(property, argumentOptions));
					break;
				}
				default: {
					throw new Error("Invalid argument type");
				}
			}
		}
		schema.args(...(argsSchemas as []));
	}
	if (options.validateReturns) {
		if (!options.returnTypeOptions) throw new Error("Function property has no returnTypeOptions");
		schema.returns(propertySchemaFrom(property, options.returnTypeOptions.type, options.returnTypeOptions).schema);
	} */

	return schema;
}

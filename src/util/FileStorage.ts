import fs from "fs";
import serialize from "serialize-javascript";
/* function deserialize(data: string): any {
	const sandbox = new VM({
		timeout: 10000,
		sandbox: {
			serialized: data,
		},
	})
	const deserialized = sandbox.run("eval('(' + serialized + ')')");
	return deserialized;
} */
function deserialize(data: string): any {
	const sandbox = { run: eval };
	const deserialized = sandbox.run("eval('(' + serialized + ')')");
	return deserialized;
}
export class FileStorage {
	private dir: string = process.env.PARZIVAL_TEMP_STORAGE_DIR || `${__dirname}/../../temp/.parzival`;
	private dataMap: Map<string, any> = new Map(); // store -> data

	constructor() {
		if (!fs.existsSync(this.dir)) {
			fs.mkdirSync(this.dir, { recursive: true });
		}
		// Clear all stores
		const files = fs.readdirSync(this.dir);
		for (const file of files) {
			fs.unlinkSync(`${this.dir}/${file}`);
		}
	}
	public save(store: string, key: string, value: any) {
		const storeData = this.getStore(store);
		storeData[key] = value;
		this.saveStore(store, storeData);
	}
	public exists(store: string, key: string) {
		const storeData = this.getStore(store);
		return storeData[key] !== undefined;

	}
	public load(store: string, key: string) {
		const storeData = this.getStore(store);
		return storeData[key];
	}
	private getStore(store: string) {
		const file = this.getFile(store);
		if (!this.dataMap.has(store)) {
			/* if (fs.existsSync(file)) {
				const data = fs.readFileSync(file, "utf8");
				const deserialized = deserialize(data);
				this.dataMap.set(store, deserialized);
			} else { */
			this.dataMap.set(store, {});
			//}
		}
		return this.dataMap.get(store);

	}
	private saveStore(store: string, data: any) {
		/* const file = this.getFile(store);
		const serialized = serialize(data); */
		this.dataMap.set(store, data);
		//fs.writeFileSync(file, serialized);
	}
	private getFile(store: string) {
		return `${this.dir}/${store}.js`;
	}
}
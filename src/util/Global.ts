import { FileStorage } from "./FileStorage";

export class Global {
	static getGlobalVariable(): any {
		return global;
	}
	static getFileStorage(): FileStorage {
		if (!Global.getGlobalVariable().parzival_temp_storage) {
			Global.getGlobalVariable().parzival_temp_storage = new FileStorage();
		}
		return Global.getGlobalVariable().parzival_temp_storage;
	}
}

export function getGlobalVariable(): any {
	return Global.getGlobalVariable();
}
export function getFileStorage(): FileStorage{
	return Global.getFileStorage();
}
import { PairDif } from "../types/Util";
import { getGlobalVariable, getFileStorage } from "./Global";

type SerializableKeyStorage = { boundObject: string, metadata: { k: string, v: unknown }[], selfFlagged: boolean }[];
type KeyStorageContent = { metadata: { k: string, v: unknown }[], selfFlagged: boolean };
export class MetadataStorage {
	// Array of key-value pairs of object names and their properties as a string array
	private keyStorage: Map<string, KeyStorageContent> = new Map();
	// TODO: Add a way to use reflection
	private usingReflection: boolean = false;
	private constructor() {
		const fileStorage = getFileStorage();
		if (fileStorage.exists("metadata", "metadataStorage")) {
			const serializedKeyStorage = fileStorage.load("metadata", "metadataStorage");
			if (serializedKeyStorage) {
				this.keyStorage = this.retrievefromSerializable(serializedKeyStorage);
			}
		}
	}
	public static getStorage(): MetadataStorage {
		if (!getGlobalVariable().parzivalMetadataStorage) {
			getGlobalVariable().parzivalMetadataStorage = new MetadataStorage();
		}
		return getGlobalVariable().parzivalMetadataStorage;
	}
	private persist(): void {
		const fileStorage = getFileStorage();
		if (!this.usingReflection) {
			const data = this.serializeKeyStorage();
			fileStorage.save("metadata", "metadataStorage", data);
		}
		else {
			const data = this.serializeReflectKeyStorage();
			fileStorage.save("metadata", "metadataStorage", data);
		}
	}
	private serializeKeyStorage(): SerializableKeyStorage {
		return Array.from(this.keyStorage.entries()).map(([boundObject, { metadata: keys, selfFlagged }]) => ({ boundObject, metadata: keys, selfFlagged }));
	}
	private serializeReflectKeyStorage(): SerializableKeyStorage {
		// TODO: Test this
		return Array.from(this.keyStorage.entries()).map(([boundObject, { metadata: keys, selfFlagged }]) => ({ boundObject, metadata: keys, selfFlagged }));
	}
	private retrievefromSerializable(serialized: SerializableKeyStorage): Map<string, KeyStorageContent> {
		if (process.env.PARZIVAL_ENV === "development") console.log("Retrieving metadata from serialized data");
		const keyStorage = new Map();
		serialized.forEach(({ boundObject, metadata, selfFlagged }) => {
			keyStorage.set(boundObject, { metadata, selfFlagged });
		});
		return keyStorage;
	}
	public hasMetadata(metadataKey: string, boundObject: Object | string): boolean {
		let name = "";
		if (typeof boundObject === "string") {
			name = boundObject;
		}
		else {
			if (process.env.PARZIVAL_ENV === "development")
				console.log("Bound object", boundObject);
			name = boundObject.constructor.name;
		}
		if (!this.keyStorage.has(name)) {
			return false;
		}
		return this.keyStorage.get(name)!.metadata.some((p) => p.k === metadataKey);

	}
	public addMetadata(metadataKey: string, metadataValue: any, boundObject: Object | string) {
		if (process.env.PARZIVAL_ENV === "development") console.log("Adding metadata to object", boundObject, metadataKey, metadataValue);
		let name = "";
		if (typeof boundObject === "string") {
			name = boundObject;
		}
		else {
			name = boundObject.constructor.name;
		}
		if (!this.keyStorage.has(name)) {
			this.keyStorage.set(name, { metadata: [{ k: metadataKey, v: metadataValue }], selfFlagged: false });
		}
		else {
			if (this.keyStorage.get(name)!.metadata.some((p) => p.k === metadataKey)) {
				const prevSelfFlagged = this.keyStorage.get(name)!.selfFlagged;
				// Merge the metadata v
				const prevMetadata = this.keyStorage.get(name)!.metadata;
				const newMetadata = prevMetadata.map((p) => {
					if (p.k === metadataKey) {
						if (typeof p.v === "object" && typeof metadataValue === "object" && !Array.isArray(p.v) && !Array.isArray(metadataValue)) {
							// Merge the objects
							const merged = { ...p.v, ...metadataValue };
							return { k: metadataKey, v: merged };
						}
						else if (Array.isArray(p.v) && Array.isArray(metadataValue)) {
							// Merge the arrays
							const merged = [...p.v, ...metadataValue];
							return { k: metadataKey, v: merged };
						}
						else {
							// Overwrite the value
							return { k: metadataKey, v: metadataValue };
						}
					}
					return p;
				});
				this.keyStorage.set(name, { metadata: newMetadata, selfFlagged: prevSelfFlagged });
			}
			else {
				const prevSelfFlagged = this.keyStorage.get(name)!.selfFlagged;
				const metadata = this.keyStorage.get(name)!.metadata;
				this.keyStorage.set(name, { metadata: [...metadata, { k: metadataKey, v: metadataValue }], selfFlagged: prevSelfFlagged });
			}
		}
		// Persist the storage
		this.persist();
	}
	public getMetadata(metadataKey: string, boundObject: Object | string): any | null {
		let name = "";
		if (typeof boundObject === "string") {
			name = boundObject;
		}
		else {
			name = boundObject.constructor.name;
		}
		if (!this.hasMetadata(metadataKey, name)) {
			return null;
		}
		return this.keyStorage.get(name)!.metadata.find((p) => p.k === metadataKey)!.v;
	}
	public keysSelfFlag(boundObject: Object | string): void {
		let name = "";
		if (typeof boundObject === "string") {
			name = boundObject;
		}
		else {
			name = boundObject.constructor.name;
		}
		if (!this.keyStorage.has(name)) {
			this.keyStorage.set(name, { metadata: [], selfFlagged: true });
		}
		else {
			this.keyStorage.set(name, { metadata: this.keyStorage.get(name)!.metadata, selfFlagged: true });
		}
		// Persist the storage
		this.persist();
	}

	public isSelfFlagged(boundObject: Object | string): boolean {
		let name = "";
		if (typeof boundObject === "string") {
			name = boundObject;
		}
		else {
			name = boundObject.constructor.name;
		}
		if (!this.keyStorage.has(name)) {
			return false;
		}
		return this.keyStorage.get(name)!.selfFlagged;
	}
}
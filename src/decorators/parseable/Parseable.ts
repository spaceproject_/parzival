import "reflect-metadata";
import { MetadataStorage } from "../../util/MetadataStorage";


/* function parzivalGenerateSchema(): ObjectSchema {
	return objectSchemaFrom(this);
} */
export function Parseable(): ClassDecorator;
// This decorator is used to mark a class as parseable.
// It adds metadata to the class that is used by the parser to determine if a class is parseable.
// The parser will only parse classes that are marked as parseable.
export function Parseable(): ClassDecorator {
	return (target: Function) => {
		if (process.env.PARZIVAL_ENV === "development") console.log(`Marking class ${target.name} as parseable`);
		// Add the class to the metadata storage
		const storage = MetadataStorage.getStorage();
		storage.addMetadata("parzival:parseable", true, target.name);
		storage.addMetadata("parzival:parseable:name", target.name, target.name);
		// Check for duplicate class names
		if (storage.isSelfFlagged(target.name)) throw new Error(`Duplicate class name: ${target.name}`);
		else storage.keysSelfFlag(target.name);
	};
}
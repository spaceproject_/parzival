import "reflect-metadata"
import { KeyOptionsPair } from "../../types/Metadata";
import { MetadataStorage } from "../../util/MetadataStorage";
import { ValidatePropertyOptions } from "../options/ValidatePropertyOptions";

export function ValidateProperty(): PropertyDecorator;
export function ValidateProperty(options: ValidatePropertyOptions): PropertyDecorator;

export function ValidateProperty(options?: ValidatePropertyOptions): PropertyDecorator {
	if (!options) {
		return () => {
			throw new Error("ValidateProperty decorator requires options");
		}
	}
	else {
		return (target: Object, propertyKey: string | symbol) => {
			if (process.env.PARZIVAL_ENV === "development") console.log(`Marking property ${options} as validateable with options ${options} in class ${target.constructor.name}`);
			const storage = MetadataStorage.getStorage();
			storage.addMetadata("parzival:validate", true, target.constructor.name);
			const keyOptionsPair: KeyOptionsPair = [propertyKey.toString(), options];
			storage.addMetadata("parzival:validate:options", [keyOptionsPair], target.constructor.name);
			// Add the property to the metadata storage
			storage.addMetadata("parzival:keys", [propertyKey], target.constructor.name);
		}
	}
}
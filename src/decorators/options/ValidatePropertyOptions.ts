import { ComplexPropertyTypesLabel, PropertyTypesLabel, SimplePropertyTypesLabel } from "../../types/PropertyTypes";

// Generic BasePropertyOptions
export interface BasePropertyOptions {
	// The type of the property.
	// This is used to determine how to parse the property.
	type: PropertyTypesLabel;
	validateName?: boolean;
	optional?: boolean;
}

// If the type is an array also allow array options
export interface ArrayPropertyOptions<T extends PropertyTypesLabel = PropertyTypesLabel> extends BasePropertyOptions {
	type: "array";
	subTypeOptions: AutoPropertyOptions<T>;
	minLength?: number;
	maxLength?: number;
}

// If the type is an object also allow object options
export interface ObjectPropertyOptions extends BasePropertyOptions {
	type: "object";
	recurse: boolean;
	className: string;
}
// If the type is a function also allow function options
export interface FunctionPropertyOptions extends BasePropertyOptions {
	type: "function";
	validateArguments: boolean;
	validateReturns: boolean;
	recurseArguments?: boolean;
	argumentsTypeOptions?: { [key: string]: ValidatePropertyOptions };
	returnTypeOptions?: ValidatePropertyOptions;
}

// If the type is a string also allow string options
export interface StringPropertyOptions extends BasePropertyOptions {
	type: "string";
	minLength?: number;
	maxLength?: number;
	match?: RegExp;
}

// If the type is a number also allow number options
export interface NumberPropertyOptions extends BasePropertyOptions {
	type: "number";
	min?: number;
	max?: number;
}

export interface BooleanPropertyOptions extends BasePropertyOptions {
	type: "boolean";
}


export type PreValidatePropertyOptions =
	| ArrayPropertyOptions
	| ObjectPropertyOptions
	| StringPropertyOptions
	| NumberPropertyOptions
	| BooleanPropertyOptions
	| FunctionPropertyOptions;

// Selectable options based on the type
type AutoPropertyOptions<T extends PropertyTypesLabel> =
	T extends "array" ? ArrayPropertyOptions :
	T extends "object" ? ObjectPropertyOptions :
	T extends "string" ? StringPropertyOptions :
	T extends "number" ? NumberPropertyOptions :
	T extends "boolean" ? BooleanPropertyOptions :
	T extends "function" ? FunctionPropertyOptions :
	never;

// Infer the options based on the type
export type ValidatePropertyOptions = AutoPropertyOptions<PropertyTypesLabel> & BasePropertyOptions;

export type SimplePropertyTypes = string | number | boolean;
export type ComplexPropertyTypes = Array<SimplePropertyTypes> | Object | Function;
export type SimplePropertyTypesLabel = "string" | "number" | "boolean";
export type ComplexPropertyTypesLabel = "array" | "object" | "function";
export type PropertyTypes = SimplePropertyTypes | ComplexPropertyTypes;
export type PropertyTypesLabel = SimplePropertyTypesLabel | ComplexPropertyTypesLabel;
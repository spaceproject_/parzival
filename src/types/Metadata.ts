import { ValidatePropertyOptions } from "../decorators/options/ValidatePropertyOptions";
import { PairDif } from "./Util";
export type KeyOptionsPair = PairDif<string, ValidatePropertyOptions>;
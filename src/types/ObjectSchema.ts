import { ZodTypeAny } from "zod";
import { PropertyTypesLabel, SimplePropertyTypes } from "../types/PropertyTypes";
import { PairDif } from "./Util";

// PropertyTypesLabel except for "object"
type ObjectPropertyType = PropertyTypesLabel;

export interface ObjectProperty {
	propertyName: string;
	propertyType: ObjectPropertyType;
	isOptional: boolean;
	schema: ZodTypeAny;
};

export interface ObjectSchema {
	validateName: boolean;
	objectName: string;
	properties: ObjectProperty[];
	recurse: boolean;
	isOptional: boolean;
	children: PairDif<string, ObjectSchema>[];
};


// Description: Utility types
// Tuple type
// A tuple type is a type that represents a fixed-length array of elements.
export type Tuple = [any, ...any[]];

// Pair type
// A pair type is a type that represents a fixed-length array of two elements.
export type Pair<T> = [T, T];
export type PairDif<T1, T2> = [T1, T2];
